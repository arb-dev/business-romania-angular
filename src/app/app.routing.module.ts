import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ProfilCompanieComponent } from './components/profil-companie/profil-companie.component';
import { ProfilInstitutieComponent } from './components/profil-institutie/profil-institutie.component';
import { ProfilAtractieTuristicaComponent } from './components/profil-atractie-turistica/profil-atractie-turistica.component';
import { ProfilJudetComponent } from './components/profil-judet/profil-judet.component';
import { ProfilLocalitateComponent } from './components/profil-localitate/profil-localitate.component';
import { HomeComponent } from './components/home/home.component';
import { ModalLoginComponent } from './components/modals/modal-login/modal-login.component';
import { AdminComponent } from './components/admin/admin.component';
import { JudeteComponent, LocalitatiComponent } from './components/admin/child-components';
import { AtractiiTuristiceComponent } from './components/admin/child-components/atractii-turistice/atractii-turistice.component';
import { SocietatiComercialeComponent } from './components/admin/child-components/societati-comerciale/societati-comerciale.component';
import { InstitutieComponent } from './components/admin/child-components/institutie/institutie.component';
import { AdministratorSistemComponent } from './components/admin/child-components/administrator-sistem/administrator-sistem.component';
import { ReprezentantSistemComponent } from './components/admin/child-components/reprezentant-sistem/reprezentant-sistem.component';
import { ReprezentantProfilComponent } from './components/admin/child-components/reprezentant-profil/reprezentant-profil.component';
import { OportunitatiComponent } from './components/admin/child-components/oportunitati/oportunitati.component';


const routes: Routes = [
  { path: '', redirectTo: '/profil-companie', pathMatch: 'full' },
  { path: 'profil-companie', component: ProfilCompanieComponent },
  { path: 'profil-institutie', component: ProfilInstitutieComponent },
  { path: 'profil-atractie-turistica', component: ProfilAtractieTuristicaComponent },
  { path: 'profil-judet', component: ProfilJudetComponent },
  { path: 'profil-localitate', component: ProfilLocalitateComponent },
  { path: 'profil-home', component: HomeComponent },
  { path: 'admin', component: AdminComponent, children: [
      { path: 'judete', component: JudeteComponent },
      { path: 'localitati', component: LocalitatiComponent },
      { path: 'atractii-turistice', component: AtractiiTuristiceComponent },
      { path: 'societati-comerciale', component: SocietatiComercialeComponent },
      { path: 'institutie', component: InstitutieComponent },
      { path: 'administrator-sistem', component: AdministratorSistemComponent },
      { path: 'reprezentant-sistem', component: ReprezentantSistemComponent },
      { path: 'reprezentant-profil', component: ReprezentantProfilComponent },
      { path: 'oportunitati', component: OportunitatiComponent }
    ] 
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  entryComponents: [
    ModalLoginComponent
  ],
  exports: [RouterModule]
})
export class RoutingModule { }
