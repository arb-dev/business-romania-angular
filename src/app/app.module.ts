//Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RoutingModule } from './app.routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ng6-toastr-notifications';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { FooterComponent } from './components/footer/footer.component';
import { ProfilCompanieComponent } from './components/profil-companie/profil-companie.component';
import { ProfilInstitutieComponent } from './components/profil-institutie/profil-institutie.component';
import { ProfilAtractieTuristicaComponent } from './components/profil-atractie-turistica/profil-atractie-turistica.component';
import { ProfilJudetComponent } from './components/profil-judet/profil-judet.component';
import { ProfilLocalitateComponent } from './components/profil-localitate/profil-localitate.component';
import { HomeComponent } from './components/home/home.component';
import { ModalLoginComponent } from './components/modals/modal-login/modal-login.component';
import { AdminComponent } from './components/admin/admin.component';
import { JudeteComponent } from './components/admin/child-components/judete/judete.component';
import { LocalitatiComponent } from './components/admin/child-components/localitati/localitati.component';
import { AtractiiTuristiceComponent } from './components/admin/child-components/atractii-turistice/atractii-turistice.component';
import { SocietatiComercialeComponent } from './components/admin/child-components/societati-comerciale/societati-comerciale.component';
import { InstitutieComponent } from './components/admin/child-components/institutie/institutie.component';
import { AdministratorSistemComponent } from './components/admin/child-components/administrator-sistem/administrator-sistem.component';
import { ReprezentantSistemComponent } from './components/admin/child-components/reprezentant-sistem/reprezentant-sistem.component';
import { ReprezentantProfilComponent } from './components/admin/child-components/reprezentant-profil/reprezentant-profil.component';
import { OportunitatiComponent } from './components/admin/child-components/oportunitati/oportunitati.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    FooterComponent,
    ProfilCompanieComponent,
    ProfilInstitutieComponent,
    ProfilAtractieTuristicaComponent,
    ProfilJudetComponent,
    ProfilLocalitateComponent,
    HomeComponent,
    ModalLoginComponent,
    AdminComponent,
    JudeteComponent,
    LocalitatiComponent,
    AtractiiTuristiceComponent,
    SocietatiComercialeComponent,
    InstitutieComponent,
    AdministratorSistemComponent,
    ReprezentantSistemComponent,
    ReprezentantProfilComponent,
    OportunitatiComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    ToastrModule.forRoot(),
  ],
  providers: [],
  entryComponents: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
