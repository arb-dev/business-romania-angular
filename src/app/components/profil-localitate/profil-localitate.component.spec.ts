import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilLocalitateComponent } from './profil-localitate.component';

describe('ProfilLocalitateComponent', () => {
  let component: ProfilLocalitateComponent;
  let fixture: ComponentFixture<ProfilLocalitateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilLocalitateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilLocalitateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
