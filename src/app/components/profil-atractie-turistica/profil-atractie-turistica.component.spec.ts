import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilAtractieTuristicaComponent } from './profil-atractie-turistica.component';

describe('ProfilAtractieTuristicaComponent', () => {
  let component: ProfilAtractieTuristicaComponent;
  let fixture: ComponentFixture<ProfilAtractieTuristicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilAtractieTuristicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilAtractieTuristicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
