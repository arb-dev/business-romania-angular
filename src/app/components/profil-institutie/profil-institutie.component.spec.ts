import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilInstitutieComponent } from './profil-institutie.component';

describe('ProfilInstitutieComponent', () => {
  let component: ProfilInstitutieComponent;
  let fixture: ComponentFixture<ProfilInstitutieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilInstitutieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilInstitutieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
