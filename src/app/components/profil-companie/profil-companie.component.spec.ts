import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilCompanieComponent } from './profil-companie.component';

describe('ProfilCompanieComponent', () => {
  let component: ProfilCompanieComponent;
  let fixture: ComponentFixture<ProfilCompanieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilCompanieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilCompanieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
