import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReprezentantProfilComponent } from './reprezentant-profil.component';

describe('ReprezentantProfilComponent', () => {
  let component: ReprezentantProfilComponent;
  let fixture: ComponentFixture<ReprezentantProfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReprezentantProfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReprezentantProfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
