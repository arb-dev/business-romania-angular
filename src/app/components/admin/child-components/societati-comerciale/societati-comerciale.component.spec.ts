import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocietatiComercialeComponent } from './societati-comerciale.component';

describe('SocietatiComercialeComponent', () => {
  let component: SocietatiComercialeComponent;
  let fixture: ComponentFixture<SocietatiComercialeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocietatiComercialeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocietatiComercialeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
