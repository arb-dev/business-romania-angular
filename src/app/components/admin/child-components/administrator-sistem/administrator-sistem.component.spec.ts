import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministratorSistemComponent } from './administrator-sistem.component';

describe('AdministratorSistemComponent', () => {
  let component: AdministratorSistemComponent;
  let fixture: ComponentFixture<AdministratorSistemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministratorSistemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministratorSistemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
