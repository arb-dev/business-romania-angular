import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JudeteComponent } from './judete.component';

describe('JudeteComponent', () => {
  let component: JudeteComponent;
  let fixture: ComponentFixture<JudeteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JudeteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JudeteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
