import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReprezentantSistemComponent } from './reprezentant-sistem.component';

describe('ReprezentantSistemComponent', () => {
  let component: ReprezentantSistemComponent;
  let fixture: ComponentFixture<ReprezentantSistemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReprezentantSistemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReprezentantSistemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
