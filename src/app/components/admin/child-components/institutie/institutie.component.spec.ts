import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitutieComponent } from './institutie.component';

describe('InstitutieComponent', () => {
  let component: InstitutieComponent;
  let fixture: ComponentFixture<InstitutieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstitutieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitutieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
