import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OportunitatiComponent } from './oportunitati.component';

describe('OportunitatiComponent', () => {
  let component: OportunitatiComponent;
  let fixture: ComponentFixture<OportunitatiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OportunitatiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OportunitatiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
