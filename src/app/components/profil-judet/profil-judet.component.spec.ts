import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilJudetComponent } from './profil-judet.component';

describe('ProfilJudetComponent', () => {
  let component: ProfilJudetComponent;
  let fixture: ComponentFixture<ProfilJudetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilJudetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilJudetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
